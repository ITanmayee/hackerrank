"""

You are given an integer array and a number K. You have to tell if there exists i,j,k in the given array such that ar[i]+ar[j]+ar[k]=K, i≠j≠k.

"""

# using two pointers technique
test_cases = int(input())

def tripletSum(size, arr, total):
    for i in range(size - 2):
        p1 = i+1
        p2 = size - 1
        while(p1 < p2):
            if arr[i] + arr[p1] + arr[p2] == total:
                return "true"
            elif arr[i] + arr[p1] + arr[p2] < total:
                p1 += 1
            else:
                p2 -= 1
    return "false"
    
for i in range(test_cases):

    size, total = map(int, input().split())
    arr = list(map(int, input().split()))
    arr.sort()
    print(tripletSum(size, arr, total))
    
