# Dan is playing a video game in which his character competes in a hurdle race. Hurdles are of varying heights, and Dan has a maximum height he can jump. There is a magic potion he can take that will increase his maximum height by 1 unit for each dose. How many doses of the potion must he take to be able to jump all of the hurdles.

# Given an array of hurdle heights 'height', and an initial maximum height Dan can jump, 'k' , determine the minimum number of doses Dan must take to be able to clear all the hurdles in the race.

def hurdleRace(k, height):
    jumps = max(height) - k
    if jumps > 0:
        return (max(height) - k)
    else:
        return 0


print(hurdleRace(4, [1, 6, 3, 5, 2]))
print(hurdleRace(7, [2, 5, 4, 5, 2]))

