"""

Given an array of unique elements, construct a Binary Search Tree and check if its a Full Binary Tree [FBT]. A FBT is one in which each node is either a leaf or possesses exactly 2 child nodes.

"""

class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None
        
class Tree:
    def createNode(self, data):
        return Node(data)
    
    def insert(self, node, data):
        if node is None:
            return Node(data)
        if data > node.data:
            node.right = self.insert(node.right, data)
        else:
            node.left = self.insert(node.left, data)
        return node
    
    def isFullTree(self, root):
        if root.data is None:
            return True
        if root.left is None and root.right is None:
            return True
        if root.left is not None and root.right is not None:
            return self.isFullTree(root.left) and self.isFullTree(root.right)
        return False
    
test_cases = int(input())

tree = Tree()

for _ in range(test_cases):
    size = int(input())
    elems = list(map(int, input().split()))
    root = tree.createNode(elems[0])
    
    for i in elems[1:]:
        tree.insert(root, i)
        
    print(tree.isFullTree(root))
        
