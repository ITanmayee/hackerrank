# Count the number of trailing 0s in factorial of a given number.

def findTrailingZeros(num):
    exp = 0
    base = 5
    while num / base >= 1:
        exp += num // base
        base *= 5
    return exp

test_cases = int(input())

for i in range(test_cases):
    num = int(input())
    print(findTrailingZeros(num))
