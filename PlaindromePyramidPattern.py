N = int(input())

alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def pattern(n , alpha) :
    s = alpha[:n]
    r = (s[::-1])[1:]
    return (' '.join(s + r))

for i in range(1 , N + 1) :
    print(pattern(i , alpha))
