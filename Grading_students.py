# Every student receives a grade in the inclusive range from 0 to 100 .Any grade less than 40 is a failing grade.
# If the difference between the grade and the next multiple of 5 is less than 3, round grade up to the next multiple of 5.
# If the value of grade is less than 38 , no rounding occurs as the result will still be a failing grade.

def gradingStudents(grades_count , grades):
    next_multiple = [((grades[i] // 5) + 1) * 5 for i in range(grades_count) ]
    return [next_multiple[i]  if ((grades[i] >= 38) and (next_multiple[i] - grades[i] < 3)) else grades[i] for i in range(grades_count)]

print(gradingStudents(4,[73,67,38,33]))
print(gradingStudents(2,[37,38]))

