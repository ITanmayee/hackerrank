#Print mirror image of right-angled triangle using '*'. 

test_cases = int(input())

for i in range(test_cases):
    num = int(input())
    print("Case #" + str(i + 1) + ':')
    for i in range(1, num+1):
        print((' ' * (num - i)) + ('*' * i))
        