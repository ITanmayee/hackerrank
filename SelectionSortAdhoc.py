# Implement Selection Sort and print the index which gets swapped at each step.

test_cases = int(input())

def selectionSort(size, arr):
    for i in range(size-1,0,-1):
        maxi, ind = arr[0], 0
        for j in range(i + 1):
            if arr[j] > maxi:
                maxi, ind = arr[j], j
        arr[ind], arr[i] = arr[i], arr[ind]
        print(ind, end = ' ')      
    print()
    
for i in range(test_cases):
    size = int(input())
    arr = list(map(int, input().split()))
    
    selectionSort(size, arr)
    
    
        
            
            