# Dothraki are planning an attack to usurp King Robert's throne. King Robert learns of this conspiracy from Raven and plans to lock the single door through which the enemy can enter his kingdom.
# But, to lock the door he needs a key that is an anagram of a palindrome. He starts to go through his box of strings, checking to see if they can be rearranged into a palindrome.


def gameOfThrones(anagram):
    odds = [anagram.count(i) for i in set(anagram) if  anagram.count(i) % 2 != 0]
    if len(odds) <= 1 :
        return "YES"
    else :
        return "NO"

print(gameOfThrones("aaabbbb"))
print(gameOfThrones("cdefghmnopqrstuvw"))
print(gameOfThrones("cdcdcdcdeeeef"))

# A set of characters can form a palindrome if at most one character occurs odd number of times and all characters occur even number of times.
