# Given 2 numbers - a and b, evaluate ab.



test_cases = int(input())

def checkbits(N, i):
    return N & (1 << i)

def ApowB(a, b):
    place_value, power = a % 1000000007, 1
    while b != 0:
        if b & 1:
            power = (power * place_value) % 1000000007
        place_value = (place_value * place_value) % 1000000007
        b = b >> 1
    return power

for i in range(test_cases):
    a, b  = map(int, input().split())
    
    print(ApowB(a, b))