"""
The Tower of Hanoi (also called the Tower of Brahma or Lucas') is a mathematical game or puzzle. It consists of three rods, and a number of disks of different sizes which can slide onto any rod. The puzzle starts with the disks in a neat stack in ascending order of size on one rod, the smallest at the top, thus making a conical shape.

The objective of the puzzle is to move the entire stack to another rod, obeying the following simple rules:
Only one disk can be moved at a time.
Each move consists of taking the upper disk from one of the stacks and placing it on top of another stack i.e. a disk can only be moved if it is the uppermost disk on a stack.
No disk may be placed on top of a smaller disk.
Your task is that given N disks, print the minimum number of moves required in order to solve the problem, followed by the actual moves.

Assumptions
1. The rods are named A, B and C.
2. All the disks are initially placed on rod A.
3. You have to move all the disks from rod A to rod C.

"""

test_cases = int(input())

def towerOfHanoi(disks, source, temp, dest):
    if disks == 0:
        return ''
    towerOfHanoi(disks-1, source, dest, temp)
    print("Move", disks, "from", source, "to", dest )
    towerOfHanoi(disks-1, temp, source, dest)
    
for i in range(test_cases) :
    disks = int(input())
    print((1 << disks) - 1)
    towerOfHanoi(disks, 'A', 'B', 'C')