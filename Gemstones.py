# John has collected various rocks. Each rock has various minerals embeded in it. Each type of mineral is designated by a lowercase alphabets . There may be multiple occurrences of a mineral in a rock. A mineral is called a gemstone if it occurs at least once in each of the rocks in John's collection.
# Given a list of minerals embedded in each of John's rocks, display the number of types of gemstones he has in his collection.
# For example, the array of mineral composition strings arr = [abc , abc , bc]. The minerals 'b' and 'c' appear in each composite, so there are 2 gemstones.


def gem_count(minerals , rocks ) :
    gems = []
    for m in minerals :
        count = 0
        for r in rocks :
            if m in r :
                count += 1
        if count == len(rocks) :
            gems.append(m)
    return len(gems)

def gemstones(rocks):
    all_minerals = ""
    for i in rocks :
        all_minerals += i
    unique_minerals = sorted(set(all_minerals))
    return gem_count(unique_minerals , rocks )


print(gemstones(["abcdde", "baccd", "eeabg"]))
print(gemstones(["abc" , "abc" ,"bc"]))
