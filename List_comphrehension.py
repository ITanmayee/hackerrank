# You are given three integers  X,Y,Z and  representing the dimensions of a cuboid along with an integer N . You have to print a list of all possible coordinates (i,j,k) given by  on a 3D grid where the sum of i+j+k is not equal to N.

def possible_list(x,y,z,n) :
    return ([ [ i, j, k] for i in range( x + 1) for j in range( y + 1) for k in range(z + 1)if ( ( i + j + k ) != n )])

print(possible_list(1,1,1,2))
