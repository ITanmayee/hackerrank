# Lisa just got a new math workbook. A workbook contains exercise problems, grouped into chapters. Lisa believes a problem to be special if its index (within a chapter) is the same as the page number where it's located. The format of Lisa's book is as follows:
# There are  chapters in Lisa's workbook, numbered from 1 to n .
# The 'i'th chapter has arr[i] problems, numbered from 1 to arr[i].
# Each page can hold up to 'k' problems. Only a chapter's last page of exercises may contain fewer than 'k' problems.
# Each new chapter starts on a new page, so a page will never contain problems from more than one chapter.
#  The page number indexing starts at 1.


def get_no_of_pages (k , prob) :
    if prob % k == 0 :
        pages_per_chp = prob // k
    else :
        pages_per_chp = prob // k + 1
    return [pages_per_chp , prob]

def arrange_problems (k , pages) :
    total_problems = list(range(1, pages[1] + 1))
    return [total_problems[ k * i : k  * (1 + i)] for i in range( pages[0])]

def workbook(n, k, arr):
    no_of_pages = [get_no_of_pages(k , i) for i in arr]
    arranged_problems = [arrange_problems(k , i) for i in no_of_pages]
    work_book = []
    for i in range(len(arranged_problems)) :
        for j in range(len(arranged_problems[i])):
            work_book.append(arranged_problems[i][j])
    special_problem = 0
    for i in range(len(work_book)) :
        if (i + 1) in work_book[i] :
            special_problem += 1
    return special_problem


print(workbook(5 , 3 , [4 , 2 , 6 , 1 ,10]))
print(workbook(25 , 10 , [1, 29, 94, 15, 87, 100, 20, 55, 100, 45, 82, 80, 100, 100, 3, 53, 100, 2, 100, 100, 100, 100, 100, 100, 1]))



