# Consider the following:
# A string,'s' , of length 'n' where s = C0 C1 C2 .... C(n - 1_.
# An integer,'k' , where 'k' is a factor of 'n'.
# We can split  's' into (n / k) subsegments where each subsegment, 'Ti', consists of a contiguous block of 'k' characters in 's'. Then, use each 'Ti' to create string 'Ui' such that:
# The characters in 'Ui' are a subsequence of the characters in 'Ti'.
# Any repeat occurrence of a character is removed from the string such that each character in 'Ui' occurs exactly once. In other words, if the character at some index 'j' in 'Ti' occurs at a previous index < j  in , 'Ti' then do not include the character in string 'Ui'.
# Given 's' and 'k', print (n / k) lines where each line 'i' denotes string 'Ui'.


def non_distinct_characters(s) :
    l = []
    for i in s :
        if i not in l :
            l.append(i)
    return ''.join(l)

def merge_the_tools(string, k):
    size = len(string)
    subsegments = [string[(i * k) : (i + 1) * k] for i in range(size // k)]
    non_distinct_strings = [non_distinct_characters(i) for i in subsegments]
    for i in non_distinct_strings :
        print(i)
    return ""

print(merge_the_tools("AABCAAADA" , 3))
