# Maria plays college basketball and wants to go pro. Each season she maintains a record of her play. She tabulates the number of times she breaks her season record for most points and least points in a game. Points scored in the first game establish her record for the season, and she begins counting from there.
# Given Maria's scores for a season, find and print the number of times she breaks her records for most and least points scored during the season.


def breakingRecords(scores):
    low = high = scores[0]
    min_count = max_count = 0
    for i in scores[1:]:
        if i > high:
            max_count += 1
            high = i
        if i < low:
            min_count += 1
            low = i
    return max_count, min_count

print(breakingRecords([10, 5, 20, 20, 4, 5, 2, 25, 1]))
print(breakingRecords([3, 4, 21, 36, 28, 35, 5, 24, 42]))

