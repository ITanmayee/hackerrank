"""

There are 2 teams, each having N players. There will be N rounds played between the 2 teams. In every round, a player from team A plays against a player from team B. The more powerful player wins the game. Given the strength of the players of both the teams, you have to find the maximum number of rounds team A can win. Note that a player cannot play more than 1 round.

"""

def merge(arr, low, mid, high):
    temp = [0] * (high - low + 1)
    p1, p2, indx = low, mid+1, 0
    
    while (p1 <= mid) and (p2 <= high):
        if arr[p1] < arr[p2]:
            temp[indx] = arr[p1]
            p1 += 1
        else:
            temp[indx] = arr[p2]
            p2 += 1
        indx += 1
        
    while (p1 <= mid):
        temp[indx] = arr[p1]
        indx += 1
        p1 += 1
            
    while(p2 <= high):
        temp[indx] = arr[p2]
        indx += 1
        p2 += 1
            
    for i in range(low, high + 1):
        arr[i] = temp[i - low]
        
def mergeSort(arr, low, high):
    if (high > low):
        mid = (low + high)// 2
        mergeSort(arr, low, mid)
        mergeSort(arr, mid+1, high)
        merge(arr, low, mid, high)

test_cases = int(input())

for i in range(test_cases):
    size = int(input())
    teamA = list(map(int, input().split()))
    teamB = list(map(int, input().split()))
    
    
    mergeSort(teamA, 0, size-1)
    mergeSort(teamB, 0, size-1)
    
    
    A, B, wins = 0, 0, 0 
    
    while (A < size) and (B < size) :
        
        if teamA[A] > teamB[B]:
            A += 1
            B += 1
            wins += 1
            
        else:
            A += 1
            
    print(wins)
        
