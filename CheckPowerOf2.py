#Given a number, check if it is a power of 2. 

test_cases = int(input())

for i in range(test_cases) :
    num = int(input())
    sqr = "False"
    for i in range(64):
        if ((1<<i) == num):
            sqr = "True"
            break
    print(sqr)
    