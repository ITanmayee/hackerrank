"""

Watson likes to challenge Sherlock's math ability. He will provide a starting and ending value that describe a range of integers, inclusive of the endpoints. 
Sherlock must determine the number of square integers within that range(both the integers are exclusive).

"""

import math

def squares(a, b):
    a_root = math.sqrt(a)
    b_root = math.sqrt(b)
    
    return math.floor(b_root) - math.ceil(a_root) + 1
    
