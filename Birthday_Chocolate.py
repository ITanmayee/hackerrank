#  Lily has a chocolate bar that she wants to share it with Ron for his birthday. Each of the squares has an integer on it. She decides to share a contiguous segment of the bar selected such that the length of the segment matches Ron's birth month and the sum of the integers on the squares is equal to his birth day. You must determine how many ways she can divide the chocolate.

 
def birthday(s, day, month):
      return len([(s[i:i+month]) for i in range(len(s) - month + 1) if sum(s[i:i+month]) == day])
   
print(birthday([1,2,1,3,2],3,2))
print(birthday([1,1,1,1,1,1],3,2)) 

