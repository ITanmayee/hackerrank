# Given 2 matrices, find the product.

test_cases = int(input())

def transpose(row, col, mat):
    trans = [[0 for i in range(row)] for j in range(col)]
    for i in range(row):
        for j in range(col):
            trans[j][i] = mat[i][j]
    return trans

def multiply(mat1, mat2):
    return sum([mat1[i]*mat2[i] for i in range(len(mat1))])

for i in range(test_cases):
    row1, col1 = map(int, input().split())
    mat1 = [list(map(int, input().split())) for i in range(row1)]
    row2, col2 = map(int, input().split())
    mat2 = [list(map(int, input().split())) for i in range(row2)]
    product = [[0 for i in range(col2)] for i in range(row1)]
    
    mat2 = transpose(row2, col2, mat2)
    
    for i in range(row1):
        for j in range(col2):
            product[i][j] = str(multiply(mat1[i], mat2[j]))
    
    for i in range(row1):
        print(' '.join(product[i]))
                               
                            
    
    
    
    
