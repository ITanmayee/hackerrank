# When you select a contiguous block of text in a PDF viewer, the selection is highlighted with a blue rectangle. In this PDF viewer, each word is highlighted independently.In this challenge, you will be given a list of letter heights in the alphabet and a string. Using the letter heights given, determine the area of the rectangle highlight in (mm ^ 2) assuming all letters are '1 mm' wide.

def designerPdfViewer(h, word):
    alphabets  = "abcdefghijklmnopqrstuvwxyz"
    max_height = max([h[alphabets.index(i)] for i in word])
    return max_height * len(word)

print(designerPdfViewer([1, 3, 1, 3, 1, 4, 1, 3, 2, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5] , "abc"))
print(designerPdfViewer([1, 3, 1, 3, 1, 4, 1, 3, 2, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 7] , "zaba"))

