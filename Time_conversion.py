# Given a time in -hour AM/PM format, convert it to military (24-hour) time.

# Note: Midnight is 12:00:00AM on a 12-hour clock, and 00:00:00 on a 24-hour clock. Noon is 12:00:00PM on a 12-hour clock, and 12:00:00 on a 24-hour clock.

def timeConversion(s):
    hr = s[:2]
    min = s[3:5]
    sec = s[6:8]
    time = s[8:10]
    if time == "AM" and  hr == "12":
        hr = "00"
    if time == "PM" and int(hr) < 12:
        hr = int(hr) + 12
    return str(hr)+":"+min+":"+sec

print(timeConversion("07:45:35PM"))
print(timeConversion("12:40:22AM"))
