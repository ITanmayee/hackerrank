# Sami's spaceship crashed on Mars! She sends a series of SOS messages to Earth for help.
# Letters in some of the SOS messages are altered by cosmic radiation during transmission. Given the signal received by Earth as a string, 's', determine how many letters of Sami's SOS have been changed by radiation.
# For example, Earth receives SOSTOT. Sami's original message was SOSSOS. Two of the message characters were changed in transit.


def marsExploration(s):
    sami_mssg = "SOS" * (len(s) // 3)
    altered_mssg = 0
    for i in range(len(s)) :
        if s[i] != sami_mssg[i] :
            altered_mssg += 1
    return altered_mssg

print(marsExploration("SOSSPSSQSSOR"))
print(marsExploration("SOSSOT"))
print(marsExploration("SOSSOSSOS"))
