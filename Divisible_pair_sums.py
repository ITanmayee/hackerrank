# You are given an array of 'n' integers, ar = [ar[0],ar[1],....ar[n-1] , and a positive integer,'k' . Find and print the number of (i,j) pairs where i < j and ar[i] + ar[j] is divisible by 'k'.


def divisibleSumPairs(n, k, ar):
    return len([(ar[i]+ar[j]) for i in range(n) for j in range(n) if (i < j) and ((ar[i]+ar[j]) % k) == 0])  

print(divisibleSumPairs(6, 3, [1,3,2,6,1,2]))
print(divisibleSumPairs(5, 3, [2, 8, 6, 8, 4]))
