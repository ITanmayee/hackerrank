"""
Given a 2D square matrix, rotate the matrix by 90 degrees in clockwise manner.
Note: Try to solve it by first scanning the matrix, then do an in-place rotation and then print the rotated matrix.

"""

test_cases = int(input())

def transpose(size, matrix):
    trans = [[0 for i in range(size)] for j in range(size)]
    for i in range(size):
        for j in range(size):
            trans[i][j] = str(matrix[j][i])
    return trans

for i in range(test_cases):
    size = int(input())
    matrix = [list(map(int, input().split())) for i in range(size)]
    print("Test Case #" + str(i+1) + ':')

    for i in transpose(size, matrix[::-1]):
        print(' '.join(i))  
