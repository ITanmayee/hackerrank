# Given a sequence of 'n' integers, p(1), p(2), p(3) ..........p(n)  where each element is distinct and satisfies 1 <= p(x) <= n. For each 'x' where 1 <= x <= n, find any integer 'y' such that p(p(y)) = x and print the value of 'y' on a new line.


def permutationEquation(p):
    p.insert(0,0)
    y = []
    for i in range(1, len(p)) :
        ind = p.index(i)
        y.append(p.index(ind))
    return y

print(permutationEquation([2, 3, 1]))
print(permutationEquation([4, 3, 5, 1, 2]))

