# Brie's Drawing teacher asks her class to open their books to a page number. Brie can either start turning pages from the front of the book or from the back of the book. She always turns pages one at a time. When she opens the book, page 1  is always on the right side.
# When she flips page 1 , she sees pages 2 and 3. Each page except the last page will always be printed on both sides. The last page may only be printed on the front, given the length of the book. If the book is 'n' pages long, and she wants to turn to page 'p', what is the minimum number of pages she will turn? She can start at the beginning or the end of the book.
# Given 'n' and 'p', find and print the minimum number of pages Brie must turn in order to arrive at page 'p'.

def pageCount(n, p):
    page0 = 0
    page1= 1
    front_set = [[page0 + (i*2),page1 + (i*2)] for i in range((n//2) + 1)]
    front_count = sum([i for i in range(len(front_set)) if p in front_set[i]])
    reverse_set = front_set[::-1]
    reverse_count =  sum([i for i in range(len(reverse_set)) if p in reverse_set[i]])
    return min([front_count,reverse_count])

print(pageCount(6, 2))
print(pageCount(5, 4))


