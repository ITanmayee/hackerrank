# Your local library needs your help! Given the expected and actual return dates for a library book, create a program that calculates the fine (if any). The fee structure is as follows:
# If the book is returned on or before the expected return date, no fine will be charged (i.e.: fine = 0).
# If the book is returned after the expected return day but still within the same calendar month and year as the expected return date, fine = 15 hackos * (the no of days late) .
# If the book is returned after the expected return month but still within the same calendar year as the expected return date, the fine = 500 hackos * (the no of months late).
# If the book is returned after the calendar year in which it was expected, there is a fixed fine of 10000 hackos.
# Charges are based only on the least precise measure of lateness. For example, whether a book is due January 1, 2017 or December 31, 2017, if it is returned January 1, 2018, that is a year late and the fine would be 10,000 hackos.


def libraryFine(day1, month1, year1, day2, month2, year2):
    if year1 > year2:
        return 10000
    if year1 == year2:
        if month1 > month2:
            return (month1 - month2) * 500
        if month1 == month2 and day1 > day2:
            return (day1 - day2) * 15
    return 0


print(libraryFine(9, 6, 2015, 6, 6, 2015))
