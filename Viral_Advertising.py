# HackerLand Enterprise is adopting a new viral advertising strategy. When they launch a new product, they advertise it to exactly 5 people on social media.
 On the first day, half of those 5 people (i.e., floor(5/2) == 2) like the advertisement and each shares it with 3 of their friends. At the beginning of the second day, floor(5/2) * 3 = 2 * 3 = 6 people receive the advertisement.
# Each day, floor(recipents / 2) of the recipients like the advertisement and will share it with 3 friends on the following day. Assuming nobody receives the advertisement twice, determine how many people have liked the ad by the end of a given day, beginning with launch day as day 1.

def viralAdvertising(days):
    shared = 5
    liked = shared // 2
    cumulative = liked
    i = 1
    while i < days :
        shared = liked * 3
        liked = shared // 2
        cumulative = cumulative + liked
        i = i + 1
    return cumulative

print(viralAdvertising(3))
print(viralAdvertising(49))

