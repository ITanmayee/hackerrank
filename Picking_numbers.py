# Given an array of integers, find and print the maximum number of integers you can select from the array such that the absolute difference between any two of the chosen integers is less than or equal to 1. For example, if your array is a = [1,1,2,2,4,4,5,5,5], you can create two subarrays meeting the criterion:[1,1,2,2]  and [4,4,5,5,5]. The maximum length subarray has 5 elements.


def pickingNumbers(a):
    count = [0] * (max(a) + 1)
    for i in a:
        count[i] += 1
        count[i - 1] += 1
    return max(count)


print(pickingNumbers([1,2,2,3,1,2]))
print(pickingNumbers([5,6,5,3,3,1]))
