"""

We say that a string contains the word hackerrank if a subsequence of its characters spell the word hackerrank. Remeber that a subsequence maintains the order of characters selected from a sequence.

More formally, let  be the respective indices of h, a, c, k, e, r, r, a, n, k in string . If  is true, then  contains hackerrank.

For each query, print YES on a new line if the string contains hackerrank, otherwise, print NO.

"""

def hackerrankInString(s):
    hackerrank = list("hackerrank")
    found = 0
    for i in s:
        if i == hackerrank[found]:
            hackerrank[found] = '*'
            found += 1
        if found == 10:
            break
    if hackerrank.count('*') == 10:
        return "YES"
    return "NO"
        
