# Print hollow diamond pattern using '*'. See examples for more details.

test_cases = int(input())

def place_stars(pattern, positions):
    for i in positions:
        pattern[i] = '*'
    return ''.join(pattern)

def star_positions(lines):
    stars = [[lines]]
    return stars + [[lines-i, lines+i] for i in range(1, lines+1)]

def make_pattern(size):
    pattern = [' ' * (2*(size+1)-1) for i in range(size+1)]
    stars = star_positions(size)
    line = 0
    
    for star in stars:
        pattern[line] = place_stars(list(pattern[line]), star)
        print(pattern[line])
        line += 1
        
    for i in pattern[:-1][::-1]:
        print(i)
        
for i in range(test_cases):
    n = int(input())
    
    print('Case #' + str(i + 1) + ':')
    make_pattern( n // 2)
    
        
