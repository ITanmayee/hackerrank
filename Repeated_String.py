# Lilah has a string, 's', of lowercase English letters that she repeated infinitely many times.
# Given an integer, 'n', find and print the number of letter a's in the first 'n' letters of Lilah's infinite string.


def repeatedString(s, n):
    if s == 'a' :
        return n
    else :
        string_size = len(s)
        A_freq = s.count('a')
        if  n % string_size == 0 :
            return A_freq * (n // string_size)
        else :
            extra_string = s [ : (n % string_size)]
            extra_A = extra_string.count('a')
            return (A_freq * (n // string_size)) + extra_A


print(repeatedString("aba", 10))
print(repeatedString("a", 1000000000000))
print(repeatedString("kmretasscityylpdhuwjirnqimlkcgxubxmsxpypgzxtenweirknjtasxtvxemtwxuarabssvqdnktqadhyktagjxoanknhgilnm", 736778906400))


