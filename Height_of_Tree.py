"""

Given an array of unique elements, construct a Binary Search Tree and find the height of the tree.

"""


class Node:
    def __init__(self, data): 
        self.data = data  
        self.left = None  
        self.right = None 
        

    def __str__(self):
        return str(self.info) 

class BinarySearchTree:
    def __init__(self): 
        self.root = None

    def create(self, val):  
        if self.root == None:
            self.root = Node(val)
        else:
            current = self.root
         
            while True:
                if val < current.data:
                    if current.left:
                        current = current.left
                    else:
                        current.left = Node(val)
                        break
                elif val > current.data:
                    if current.right:
                        current = current.right
                    else:
                        current.right = Node(val)
                        break
                else:
                    break

def maxValue(root):
    if root == None:
        return float('-inf')
    return max(root.data, max(maxValue(root.left), maxValue(root.right)))
    
def height(root):
    if root == None:
        return -1
    return 1 + max(height(root.left), height(root.right))
    
test_cases = int(input())

for i in range(test_cases):
    size = int(input())
    arr = list(map(int, input().split()))
    
    tree = BinarySearchTree()
    
    for i in arr:
        tree.create(i)
    print(height(tree.root))
