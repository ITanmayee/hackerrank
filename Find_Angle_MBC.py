# ABC is a right angled triangle . M is the mid-point of the hypoteneous AC . You are given the lengths AB and BC . Your task is to find ABC . 

import  math

def get_angle(AB , BC) :
    return round(math.degrees(math.atan2(AB,BC))) 

print(get_angle(10 , 10))
print(get_angle(1 , 10))
print(get_angle(100 , 1))


