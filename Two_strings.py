# Given two strings, determine if they share a common substring. A substring may be as small as one character.
# For example, the words "a", "and", "art" share the common substring 'a'. The words "be" and "cat" do not share a substring.


def twoStrings(s1, s2):
    common_letters = [i for i in set(s1 + s2) if i in s1 and i in s2]
    if len(common_letters) > 0 :
        return "YES"
    else :
        return "NO"


print(twoStrings("hello" , "world"))
print(twoStrings("hi" , "world"))
