# Implement Bubble Sort and print the total number of swaps involved to sort the array.

test_cases = int(input())

def bubbleSortSwaps(size, arr):
    swaps = 0
    for i in range(size):
        for j in range(size - 1):
            if arr[j] > arr[j+1]:
                arr[j], arr[j+1] = arr[j+1], arr[j]
                swaps += 1
    return swaps

for i in range(test_cases):
    size = int(input())
    arr = list(map(int, input().split()))
    print(bubbleSortSwaps(size, arr))
    