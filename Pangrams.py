# Roy wanted to increase his typing speed for programming contests. His friend suggested that he type the sentence "The quick brown fox jumps over the lazy dog" repeatedly. This sentence is known as a pangram because it contains every letter of the alphabet.
# After typing the sentence several times, Roy became bored with it so he started to look for other pangrams.
# Given a sentence, determine whether it is a pangram. Ignore case.

def pangrams(s):
    all_letters = [i.lower() for i in s if i.isalpha()]
    alphabets = "abcdefghijklmnopqrstuvwxyz "
    if len([i for i in alphabets if i in all_letters]) == 26 :
         return "pangram"
    else :
       return "not pangram"


print(pangrams("We promptly judged antique ivory buckles for the next prize"))
print(pangrams("We promptly judged antique ivory buckles for the prize"))
print(pangrams("The quick brown fox jumps over the lazy dog"))


