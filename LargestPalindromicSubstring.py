# Given a string, find the length of the largest palindromic substring.

def oddPalindrome(size, word):
    ans = 1
    for i in range(size):
        p1, p2 = i, i
        while (p1 >= 0) and (p2 < size) and (word[p1] == word[p2]):
            p1 -= 1
            p2 += 1
        ans = max(ans, p2-p1-1)
    return ans

def evenPalindrome(size, word):
    ans = 1
    for i in range(size - 1):
        p1, p2 = i, i+1
        while (p1 >= 0) and (p2 < size) and (word[p1] == word[p2]):
            p1 -= 1
            p2 += 1
        ans = max(ans, p2-p1-1)
    return ans
        
        

test_cases = int(input())

for i in range(test_cases) :
    size = int(input())
    word = input()
    print(max(oddPalindrome(size, word), evenPalindrome(size, word)))
