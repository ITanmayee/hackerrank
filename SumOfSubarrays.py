"""

Given an array of integers, answer queries of the form: [i, j] :
Print the sum of array elements from A[i] to A[j], both inclusive.

"""


def cumulativeSum(arr, size):
    ps = [0] * size
    ps[0] = arr[0]
    for i in range(1, size):
        ps[i] = ps[i-1] + arr[i]
    return ps

def total(ps, q1, q2):
    return ps[q2] - ps[q1] + arr[q1]
size = int(input())

arr = list(map(int, input().split()))

queries = int(input())

ps = cumulativeSum(arr, size)

for i in range(queries):
    q1, q2 = map(int, input().split())
    
    print(total(ps, q1, q2))
    
