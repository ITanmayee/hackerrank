/* Given a positive integer, print its binary representation. */

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>


void intoBinary(long long int  num) {
        if (num > 1) {
            intoBinary(num / 2);
        }
        printf("%lld", num % 2);
}


int main() {
    int test_cases;
    scanf("%d", &test_cases);
    void intoBinary(long long int num);
    for(int i = 0; i <test_cases; i++){
        long long int num;
        scanf("%lld", &num);
        intoBinary(num);
        printf("\n");
    }
    return 0;
}
