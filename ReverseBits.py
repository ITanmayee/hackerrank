# Given a number, reverse the bits in the binary representation (consider 32-bit unsigned data) of the number, and print the new number formed.

test_cases = int(input())

def intoBinary(num) :
    binary = []
    while num != 0:
        binary.append(num % 2)
        num = num // 2
    return ([0] * (32-len(binary))) + binary[::-1]

for i in range(test_cases):
    num = int(input())
    swapped_number, i = 0, 0
    swap_binary = intoBinary(num)[::-1]
    place_values = [(1<<i) for i in range(31, -1, -1)]
    print(sum([place_values[i]* swap_binary[i] for i in range(32)]))