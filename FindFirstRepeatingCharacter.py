# Given a string of characters, find the first repeating character.

test_cases = int(input())

for i in range(test_cases):
    word = input()
    repeats = []
    for i in range(len(word)):
        for j in range(len(word)):
            if (word[i] == word[j]) and (i != j):
                repeats.append(word[i])
                break
    
    if len(repeats) == 0:
        print('.')
    else:
        print(repeats[0])
    
