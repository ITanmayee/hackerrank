"""

Given an array of unique elements, construct a Binary Search Tree and print the Level Order of the tree.

"""

test_cases = int(input())

class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None
        
    def __str__(self):
        return str(self.data)

class Tree:
    def createNode(self, data):
        return Node(data)
    
    def insert(self, node, data):
        if node is None:
            return self.createNode(data)
        if data < node.data:
            node.left = self.insert(node.left, data)
        else:
            node.right = self.insert(node.right, data)
        return node
    
    def levelOrder(self, root):
        level_elems = []
        queue = [root]
        queue.append("NULL")
        print(root.data)
        while len(queue) != queue.count("NULL"):
            root = queue.pop(0)
            if root == "NULL":
                queue.append("NULL")
                print(' '.join(level_elems))
                level_elems = []
            else:
                if root.left is not None:
                    queue.append(root.left)
                    level_elems.append(str(root.left))
                if root.right is not None:
                    queue.append(root.right)
                    level_elems.append(str(root.right))
                      
            
tree = Tree()

for _ in range(test_cases):
    size = int(input())
    elems = list(map(int, input().split()))
    root = tree.createNode(elems[0])

    for i in elems[1:]:
        tree.insert(root, i)
        
    tree.levelOrder(root)
    print()
    
    
