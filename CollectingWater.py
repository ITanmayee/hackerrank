"""

You are given the heights of N buildings. All the buildings are of width 1 and are adjacent to each other with no empty space in between. Assume that its raining heavily, and as such water will be accumulated on top of certain buildings. Your task is to find the total amount of water accumulated.

"""

test_cases = int(input())

for i in range(test_cases):
    buildings = int(input())
    heights = list(map(int, input().split()))
    ans = 0
    r = [0] * buildings
    
    l = [heights[0]]
    r[buildings -1] = heights[buildings -1]
    
    for i in range(1, buildings - 1 ):
        l.append(max(l[i-1], heights[i]))
        
    for i in range(buildings-2, -1, -1):
        r[i] = max(r[i+1], heights[i])
        
    for i in range(buildings - 1):
        ans += min(l[i], r[i]) - heights[i]
        
    print(ans)
        
