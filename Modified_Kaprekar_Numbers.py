# A modified Kaprekar number is a positive whole number with a special property. If you square it, then split the number into two integers and sum those integers, you have the same value you started with.
# Consider a positive whole number 'n' with 'd' digits. We square 'n' to arrive at a number that is either (2 * d) digits long or ((2 * d) - 1) digits long. Split the string representation of the square into two parts, 'l' and 'r'. The right hand part, 'r' must be 'd' digits long. The left is the remaining substring. Convert those two substrings back to integers, add them and see if you get 'n'.


def is_kaprekar(num) :
    if num == 1 :
        return True
    if num >= 9 :
        square = str(num * num )
        num_digits = len(str(num))
        sqr_digits = len(square)
        right_digits = num_digits
        if  sqr_digits % 2 == 0 :
            left_digits = num_digits
        else :
            left_digits = sqr_digits - right_digits
        s1 = int(square[:left_digits])
        s2 = int(square[left_digits:])
        if s1 + s2 == num :
            return True
        return False


def kaprekarNumbers(p, q):
    kaprekar = [i for i in range(p , (q + 1)) if is_kaprekar(i) ]
    if kaprekar == [] :
        return "INVALID RANGE"
    else :
        return kaprekar

print(kaprekarNumbers(1 , 100))
print(kaprekarNumbers(400 , 700))
print(kaprekarNumbers(1000 , 10000))



