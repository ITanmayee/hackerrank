#Alice and Bob each created one problem for HackerRank. A reviewer rates the two challenges, awarding points on a scale from 1  to 100 for three categories: problem clarity, originality, and difficulty.

#Complete the function compareTriplets in the editor below. It must return an array of two integers, the first being Alice's score and the second being Bob's.


def compareTriplets(a, b):
    bob = 0
    alice = 0
    for i in range(3):
        if a[i] > b[i]:
            alice += 1
        elif a[i] < b[i]:
            bob += 1
        else:
            alice += 0
            bob += 0
    return alice,bob
            
print(compareTriplets([5,6,7],[3,6,10]))
print(compareTriplets([17,28,30],[99,16,8]))

