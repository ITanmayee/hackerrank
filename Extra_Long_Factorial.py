# The factorial of the integer 'n' , written 'n!', is defined as:
#     n! = n * (n - 1) * (n - 2) * .......* 3 * 2 * 1
# Calulate and print the factorial of a given integer .

def extraLongFactorials(n):
    if n == 1 or 0 :
        return 1
    else :
        return n * extraLongFactorials(n - 1)

print(extraLongFactorials(25))
print(extraLongFactorials(45))

