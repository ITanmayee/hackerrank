# Given the participants' score sheet for your University Sports Day, you are required to find the runner-up score. You are given  scores. Store them in a list and find the score of the runner-up. 

def get_runner(arr):   
    l = []
    for i in arr:
        if i not in l:
            l.append(i)
    l.sort()
    return (l[-2])

print(get_runner([2, 3, 6, 6, 5]))
print(get_runner([6, 6 ,6, 6, 6, 5]))
print(get_runner([-10 , 0, 10]))

