"""

Given a string, remove characters until the string is made up of any two alternating characters. When you choose a character to remove, all instances of that character must be removed. Determine the longest string possible that contains just two alternating letters.

Example

s = "abaacdabd"

Delete a, to leave bcdbd. Now, remove the character c to leave the valid string bdbd with a length of 4. Removing either b or d at any point would not result in a valid string. Return .

Given a string , convert it to the longest possible string  made up only of alternating characters. Return the length of string . If no string  can be formed, return .

"""

#!/bin/python3

import math
import os
import random
import re
import sys
from itertools import combinations

def is_alternate_word(word):
    odd_ch = [word[i] for i in range(len(word)) if i % 2 == 1]
    even_ch = [word[i] for i in range(len(word)) if i % 2 == 0]
    if len(set(odd_ch)) == 1 and len(set(even_ch)) == 1:
        return True
    return False
        
    
# Complete the alternate function below.
def alternate(s):
    alter_len = [0]
    combos = combinations(set(s), 2)
    for i in combos:
        non_del_ch = ''.join([ch if ch in i else '' for ch in s])
        if is_alternate_word(non_del_ch):
            alter_len.append(len(non_del_ch))

    return max(alter_len)
            
    
if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    l = int(input().strip())

    s = input()

    result = alternate(s)

    fptr.write(str(result) + '\n')

    fptr.close()
