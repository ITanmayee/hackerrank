"""

You have a collection of N rods. Each rod has a unique mark on it. You are given the lengths of each rod. You have to tell how many different triangles can be formed using these rods.

"""

def findnumberofTriangles(arr): 
    size = len(arr) 
    arr.sort() 
    count = 0
    for i in range(0, size - 2): 
        k = i + 2
        for j in range(i + 1, size): 
            while (k < size and arr[i] + arr[j] > arr[k]): 
                k += 1
            
            count += k - j - 1
  
    return count 

for _ in range(int(input())):
    n = int(input())
    l = list(map(int,input().split()))
    print(findnumberofTriangles(l))
            
    
