# You are given a number of sticks of varying lengths. You will iteratively cut the sticks into smaller sticks, discarding the shortest pieces until there are none left. At each iteration you will determine the length of the shortest stick remaining, cut that length from each of the longer sticks and then discard all the pieces of that shortest length. When all the remaining sticks are the same length, they cannot be shortened so discard them.
# Given the lengths of 'n' sticks, print the number of sticks that are left before each iteration until there are none left.


def cutTheSticks(arr):
    sticks_cut_count = []
    while len(arr) != 0 :
        sticks_cut_count.append(len(arr))
        length_of_cut = min(arr)
        for i in range(arr.count(length_of_cut)) :
            arr.remove(length_of_cut)
    return sticks_cut_count

print(cutTheSticks([4, 5, 10, 8, 11]))
print(cutTheSticks([5, 4, 4, 2, 2, 8]))
print(cutTheSticks([1, 2, 3, 4, 3, 3, 2, 1]))
