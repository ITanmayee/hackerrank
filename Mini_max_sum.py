# Given five positive integers, find the minimum and maximum values that can be calculated by summing exactly four of the five integers. Then print the respective minimum and maximum values as a single line of two space-separated long integers.

def miniMaxSum(arr):
    arranged_array = sorted(arr)
    print(sum(arranged_array[:-1]) , sum(arranged_array[1:]))

miniMaxSum([1, 3, 5, 7, 9])
miniMaxSum([426980153, 354802167, 142980735, 968217435, 734892650])
miniMaxSum([7, 69, 2, 221, 8974])
