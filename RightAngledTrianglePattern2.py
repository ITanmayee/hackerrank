""" Print right-angled triangle pattern with 'n' is the size of the triangle as below:

1
2 6
3 7 10
4 8 11 13
5 9 12 14 15

"""

n = int(input())

for i in range(1, n+1):
    print(i, end = " ")
    num = i
    for j in range(1, i):
        num +=  n - j
        print(num, end = " ")    
    print()
