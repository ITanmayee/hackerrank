""" 
You are given two numbers A and B. Write a program to count the number of bits to be flipped to change the number A to the number B. Flipping a bit of a number means changing a bit from 1 to 0 or vice versa.

"""

test_cases = int(input())

def checkBits(num):
    ones = 0
    while num != 0:
        num = num & (num - 1)
        ones += 1
    return ones

for i in range(test_cases):
    A, B = map(int, input().split())
    diff = (A ^ B)
    print(checkBits(diff))