#John works at a clothing store. He has a large pile of socks that he must pair by color for sale. Given an array of integers representing the color of each sock, determine how many pairs of socks with matching colors there are.

def sockMerchant(n, ar):
    colours = list(set(ar))
    is_pairing = [(ar.count(colours[i])) // 2 for i in range(len(colours)) ] 
    return sum(is_pairing)

print(sockMerchant(9, [10 ,20 ,20 ,10 ,10 ,30 ,50, 10, 20]))
print(sockMerchant(20, [4, 5, 5, 6, 6, 4, 1, 4, 4, 3, 6, 5, 3, 6, 1, 4, 5, 5, 5]))
