"""

Given an array of unique elements, construct a Binary Search Tree and print the PreOrder, InOrder and PostOrder Traversals of the tree.

"""
class Node:
    def __init__(self, data): 
        self.data = data  
        self.left = None  
        self.right = None 
        

    def __str__(self):
        return str(self.info) 

class BinarySearchTree:
    def __init__(self): 
        self.root = None

    def create(self, val):  
        if self.root == None:
            self.root = Node(val)
        else:
            current = self.root
         
            while True:
                if val < current.data:
                    if current.left:
                        current = current.left
                    else:
                        current.left = Node(val)
                        break
                elif val > current.data:
                    if current.right:
                        current = current.right
                    else:
                        current.right = Node(val)
                        break
                else:
                    break

def preOrder(root):
    if root == None:
        return
    print(root.data, end = " ")
    preOrder(root.left)
    preOrder(root.right)
    
def inOrder(root):
    if root == None:
        return
    inOrder(root.left)
    print(root.data, end = " ")
    inOrder(root.right)

def postOrder(root):
    if root == None:
        return
    postOrder(root.left)
    postOrder(root.right)
    print(root.data, end = " ")
    

test_cases = int(input())

for i in range(test_cases):
    size = int(input())
    arr = list(map(int, input().split()))
    
    tree = BinarySearchTree()
    
    for i in arr:
        tree.create(i)
    preOrder(tree.root)
    print()
    inOrder(tree.root)
    print()
    postOrder(tree.root)
    print()
    print()
