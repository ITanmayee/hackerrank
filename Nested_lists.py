# Given the names and grades for each student in a Physics class of 'N' students, store them in a nested list and print the name(s) of any student(s) having the second lowest grade.


def get_second_least_grade_student(std) :
    std.sort()
    marks = []
    for i in range(len(std)):
        marks.append(std[i][1])
    marks.sort(reverse = True)
    mark = []
    for i in range(len(marks)):
        if marks[i] not in mark:
            mark.append(marks[i])
    for i in range(len(std)):
        if (mark[-2] == std[i][1]):
             print(std[i][0])
    return 0

get_second_least_grade_student([['Harry', 37.21], ['Berry', 37.21], ['Tina', 37.2], ['Akriti', 41], ['Harsh', 39]])
