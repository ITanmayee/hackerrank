# In this challenge, you will determine whether a string is funny or not. To determine whether a string is funny, create a copy of the string in reverse e.g. abc -> cba. Iterating through each string, compare the absolute difference in the ascii values of the characters at positions 0 and 1, 1 and 2 and so on to the end. If the list of absolute differences is the same for both strings, they are funny.
# Determine whether a give string is funny. If it is, return Funny, otherwise return Not Funny.

def get_absolute_diff(values) :
    return [abs(values[i - 1] - values[i]) for i in range(1 ,len(values)) ]

def funnyString(s):
    original_word = [ord(i) for i in s]
    reverse_word = [ord(i) for i in s[::-1]]
    if (get_absolute_diff(original_word)) == (get_absolute_diff(reverse_word)) :
        return "Funny"
    else :
        return "Not Funny"

print(funnyString("lmnop"))
print(funnyString("acxz"))
print(funnyString("bcxz"))
