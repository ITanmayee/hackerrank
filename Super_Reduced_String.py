# Steve has a string of lowercase characters in range ascii['a'..'z']. He wants to reduce the string to its shortest length by doing a series of operations. In each operation he selects a pair of adjacent lowercase letters that match, and he deletes them. For instance, the string aab could be shortened to b in one operation.
# Steve's task is to delete as many characters as possible using this method and print the resulting string. If the final string is empty, print Empty String

def get_reduced_string(word) :
    letters = list(word)
    for i in range(1 , len(word)) :
        if letters[i - 1] == letters[i] :
            letters[i - 1] , letters[i] = "" , ""
    return ''.join(letters)

def is_reduced_string(word) :
    for i in range(1 , len(word)) :
        if word[i - 1] == word[i] :
            return True

def superReducedString(word):
    while is_reduced_string(word) :
        word = get_reduced_string(word)
    if word == "" :
        return "Empty String"
    else :
        return word


print(superReducedString("aaabccddd"))
print(superReducedString("baab"))
print(superReducedString("hggsssbbttrrrugeettfg"))

