#  Implement Insertion Sort and print the index at which the ith element gets inserted [i>=1].

test_cases = int(input())

def insertionSort(size, arr):
    for i in range(1, size):
        j, temp = i-1, arr[i]
        while j>=0 and arr[j]>temp :
            arr[j+1] = arr[j]
            j = j-1
        arr[j+1] = temp
        print(j+1, end = ' ' )
    print()

for i in range(test_cases):
    size = int(input())
    arr = list(map(int, input().split()))
    
    insertionSort(size, arr)