# You are given a positive integer N.
# Your task is to print a palindromic triangle of size N.
# example :
# 1
# 121
# 12321
# 1234321
# ...........

def triangle(n) :
    for i in range(1, n): #More than 2 lines will result in 0 score. Do not leave a blank line also
        print(((10 ** i) // 9) ** 2)

print(triangle(5))
print(triangle(7))
print(triangle(10))
