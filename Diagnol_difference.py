# Given a nxn square matrix, calculate the absolute difference between the sums of its diagonals.

def diagonalDifference(n, arr):
    diag1 = [arr[i][j] for i in range(n) for j in range(n) if i == j]
    diag2 = [arr[i][j] for i in range(n) for j in range(n) if i+j == n-1]
    return abs(sum(diag1)-sum(diag2))

print(diagonalDifference(3,[[1,2,3],[4,5,6],[9,8,9]]))
print(diagonalDifference(3,[[11,2,4],[4,5,6],[10,8,-12]]))                                                                                      
