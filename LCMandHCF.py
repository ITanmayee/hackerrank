# Given 2 numbers, find their LCM and HCF.

test_cases = int(input())

def hcf(num1, num2):
    rem = num1 % num2
    if rem == 0:
        return num2
    return hcf(num2, rem)

def lcm(num1, num2):
    return ((num1 * num2) // hcf(num1, num2))

for i in range(test_cases):
    num1, num2 = map(int, input().split())
    print(lcm(num1, num2), hcf(num1, num2))