# Monica wants to buy a keyboard and a USB drive from her favorite electronics store. The store has several models of each. Monica wants to spend as much as possible for the 2 items, given her budget.

# Given the price lists for the store's keyboards and USB drives, and Monica's budget, find and print the amount of money Monica will spend. If she doesn't have enough money to both a keyboard and a USB drive, print -1 instead. She will buy only the two required items.

def getMoneySpent(n , m, keyboards, drives, budget):
    total_cost = list(set([keyboards[i] + drives[j] for i in range(n)  for j in range(m)]))
    conditionl_total = [total_cost[i] for i in range(len(total_cost)) if total_cost[i] <= budget ]
    if conditionl_total == []:
        return -1
    return max(conditionl_total)

print(getMoneySpent(2, 3, [3, 1], [5, 2, 8], 10)) 
print(getMoneySpent(1, 1, [4], [5], 5))

