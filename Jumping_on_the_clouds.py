# Emma is playing a new mobile game that starts with consecutively numbered clouds. Some of the clouds are thunderheads and others are cumulus. She can jump on any cumulus cloud having a number that is equal to the number of the current cloud plus 1 or 2 . She must avoid the thunderheads. Determine the minimum number of jumps it will take Emma to jump from her starting postion to the last cloud. It is always possible to win the game.

def jumpingOnClouds(c):
    safe_cloud = [i for i in range(len(c)) if c[i] == 0]
    steps = 0
    start = safe_cloud[0]
    for i in range(len(safe_cloud)) :
        if (start + 2) in safe_cloud :
            start += 2
            steps += 1
        elif (start + 1) in safe_cloud :
            start += 1
            steps += 1
        else :
            steps += 0
    return steps

print(jumpingOnClouds([0, 0, 1, 0, 0, 1, 0]))
print(jumpingOnClouds([0 ,0 ,1 ,0 ,0 ,0 ,0 ,1 ,0 ,0 ,0 ,0 ,1 ,0 ,0 ,0 ,0 ,0 ,1 ,0 ,1 ,0 ,0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 ,0 ,0 ,1 ,0 ,1 ,0 ,0]))
