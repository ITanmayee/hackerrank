# Given the participants' score sheet for your University Sports Day, you are required to find the runner-up score. You are given  scores. Store them in a list and find the score of the runner-up.

def runner(inp):
    rank = []
    for i in inp:
        if i not in rank:
            rank.append(i)
    rank.sort()
    return rank[-2]

print(runner([2,3,6,6,5,7,7,8,4,5]))


